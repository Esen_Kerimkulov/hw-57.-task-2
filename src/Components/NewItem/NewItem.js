import React from 'react';
import './NewItem.css';

const Item = (props) => {
    return (
        <div>
            {props.lolkek.map((task, index) => {
                return (
                    <div className="newdiv" key={index}>
                        <p>
                            {task.name}
                            <span> {task.cost}</span> KGS
                            <button className="btn" onClick={() => this.props.deleteItem(task.itemName)}>{'\u2718'}</button>
                        </p>
                    </div>

                )
            })}
        </div>

    )
};



export default Item;