import React, {Fragment} from 'react';
import './AddNameCostForm.css';

const Form = (props) => {
    return (
        <Fragment>
            <input className="ListMain" onChange={props.changeName} type="text"  placeholder='Item name'/>
            <input onChange={props.changeCost} type="text" placeholder='Cost'/>
            <button className="button" onClick={props.addTask}>Add</button>
        </Fragment>
    )
};

export default Form ;