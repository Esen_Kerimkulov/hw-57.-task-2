import React, { Component } from 'react';
import './App.css';
import Form from './Components/AddNameCostForm/AddNameCostForm';
import Item from './Components/NewItem/NewItem';

class App extends Component {

  state = {
    tasks: [],
    cost: 0,
    itemName: '',
  };


  ItemName = (event) => {
    this.setState({
      itemName: event.target.value
    })
  };

  deleteItem = itemName => {
    const filteredTasks = this.state.tasks.filter(task => {
      return task.itemName !== itemName
    });
    this.setState({
      tasks: filteredTasks,
    })
  };

  changeCost = (event) => {
    this.setState({
      cost: parseInt(event.target.value)
    })
  };

  addTask = () => {
    const tasks = [...this.state.tasks];
    const newTask = {
      name: this.state.itemName,
      cost: this.state.cost
    };

    tasks.push(newTask);
    this.setState({
      tasks: tasks,
      itemName: '',
      cost: 0
    })
  };

  render() {
    return (
        <div className="App">
          <header>
            <h1 className="personal">Personal finance accounting</h1>
          </header>
          <Form
              addTask={() => this.addTask()}
              changeCost={(event) => this.changeCost(event)}
              changeName={(event) => this.ItemName(event)}
          />
          <Item lolkek={this.state.tasks} deleteItem={this.state.deleteItem} />
        </div>
    );
  }
}

export default App;